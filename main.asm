global _start

%include "words.inc"
%include "lib.inc"

%define BUFFER 256
%define STDOUT 1
%define STDERR 0
extern find_word
section .bss
    buffer: resb BUFFER

section .rodata

tl: db "too long", 0
nf: db "Word was not found", 0

section .text

_start:
    mov rdi, buffer
    mov rsi, BUFFER
    call read_line

    test rax, rax
    je .too_long

    mov rdi, rax
    mov rsi, POINTER
    call find_word

    cmp rax, 0
    jz .not_found
   
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rax, rdi
    inc rax
  
    mov rdi, rax
    jmp .print_message_and_exit

.too_long:
    mov rdi, tl
    mov rsi, STDERR
    jmp .print_message_and_exit

.not_found:
    mov rdi, nf
    mov rsi, STDERR

.print_message_and_exit:
    call print_string
    call print_newline
    call exit

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line

section .data

codes: db '0123456789'

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 
    mov rcx, -1     
    cld                             ; df backward
    repne scasb                     ; while [rdi] != al
    not rcx                         ;  RCX  0xffffffff =-1 ,result= -1-RCX
    dec rcx      
    mov rax, rcx                    ; ret = len
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, 1
    mov rax, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push 0
    push rdi
    mov rsi, rsp
    mov rax, 1 
    mov rdi, 1
    mov rdx, 1
    syscall
    add rsp, 8
    pop rdx
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, 0xA							; base
	mov rax, rdi						; x -> rax, 
	mov rdi, rsp						; alloc buff
	dec rdi 							 
	push 0 								; null dw
    sub rsp, 0x10 						; fix rsp

.loop: 									
	xor rdx, rdx 						
    div r9 								; x = x / 10; rdx = x % 10 
	add rdx, '0' 						; char -> int
	dec rdi 							
    mov [rdi], dl  						; r
    cmp rax, 0							
	je .end 							
	jmp .loop 							

.end:
	call print_string 				
	add rsp, 0x18                        ; fix rsp
    ret 


    ; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi 
    jns print_uint
    push rdi
    mov rdi, 0x2D      ; -
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret
    ; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; rdi rsi
.loop:
    mov al, byte [rdi]  
    mov dl, byte [rsi]
    cmp al, dl      
    jne .exit_zero
    inc rdi
    inc rsi
    test dl,dl      
    jnz .loop
.exit:
    xor rax,rax
    inc rax
    ret
.exit_zero:
    xor rax,rax
    ret    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push byte 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8

.loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi

    cmp r8, rsi
    je .exit_buff
    cmp rax, 0
    je .exit

    cmp rax, 0x20
    je .space
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .space

    mov byte [rdi + r8], al
    inc r8
    jmp .loop


.space:
    cmp r8, 0
    je .loop

.exit: 
    mov rax, rdi
    mov rdx, r8
    mov byte [rdi + r8], 0
    ret

.exit_buff:
    xor rax, rax
    ret
read_line:
    xor r8, r8

.loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi

    cmp r8, rsi
    je .exit_buff
    cmp rax, 0
    je .exit

    cmp rax, 0xA
    je .space

    mov byte [rdi + r8], al
    inc r8
    jmp .loop


.space:
    cmp r8, 0
    je .loop

.exit: 
    mov rax, rdi
    mov rdx, r8
    mov byte [rdi + r8], 0
    ret

.exit_buff:
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax,rax
    xor rdx ,rdx 
     
.convert:
    movzx rsi, byte [rdi]  
    test rsi, rsi           ;null
    je .done
    
    cmp rsi, 0x30            
    jl .done
    
    cmp rsi, 0x39       
    jg .done
     
    inc rdx
    sub rsi, 0x30             ; - 0 char 
    imul rax, 10            
    add rax, rsi            
    
    inc rdi                 
    jmp .convert

 
.done:
    ret         


    
    



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax,rax  
    xor rdx,rdx
    xor rsi,rsi
    xor r9,r9 
    cmp byte[rdi], 0x2D
    jne .convert
    inc r9
    inc rdi
.convert:
call parse_uint

.done:
    cmp r9, 1
    je .neg
    ret    
.neg:
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    call string_length
    pop rdi
    cmp rax, rdx
    jnb .len_over
    mov rcx, rax
.loop:
    mov r8b, [rdi + rcx]
    mov [rsi + rcx], r8b
    sub rcx, 1
    jnc .loop
    ret
.len_over:
    xor rax, rax        
    ret



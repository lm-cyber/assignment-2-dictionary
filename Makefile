COMP= nasm -f elf64



%.o: %.asm
	$(COMP) -o $@ $<
ld: main.o dict.o lib.o
	ld *.o -o main

clean:
	rm -rf *.o main


.PHONY: clean ld



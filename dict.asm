section .text
%include "lib.inc"
global find_word

find_word:
	.loop: 
        cmp rsi, 0
		je .not
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		test rax, rax
		jnz .find 
		mov rsi, [rsi]
		jmp .loop
	.find:
		mov rax, rsi
		ret
	.not:
        xor rax, rax
        ret

